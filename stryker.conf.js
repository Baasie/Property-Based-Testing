module.exports = function(config){
    config.set({
        files: [
            // Add your files here, this is just an example:
            { pattern: 'src/main/javascript/**/*.js', mutated: true, included: false},
            'src/test/javascript/**/*.js'
        ],
        testRunner: 'mocha',
        testFramework: 'mocha',
        coverageAnalysis: 'perTest',
        reporter: ['html', 'progress']
    });
};