package com.baasie.pbt.part3;

import com.google.common.base.Preconditions;

public class ProductName {
    private static final String LOWERCASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPERCASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMBERS = "0123456789";
    private static final String SPACE = " ";
    public static final String ALL_MY_CHARS = LOWERCASE_CHARS
            + UPPERCASE_CHARS + NUMBERS + SPACE;
    public static final int CAPACITY = 40;
    private static final String REGEX = "^[^" + ALL_MY_CHARS + "]+$";

    private String value;

    public ProductName(String name) {
        Preconditions.checkArgument(name.length() <= CAPACITY, "Name %s is to long.", name);
        Preconditions.checkArgument(!name.matches(REGEX), "Name %s has invalid characters.", name);
        this.value = name;
    }

    public String name() {
        return value;
    }

}
