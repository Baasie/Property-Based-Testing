package com.baasie.pbt.part3;

import com.google.common.base.Preconditions;

import java.math.BigInteger;

public class Grams {

    private BigInteger value;

    public Grams(BigInteger weight) {
        Preconditions.checkArgument(weight.compareTo(BigInteger.ZERO) > 0, "Grams is equal or smaller than zero.");
        this.value = weight;
    }

    public boolean greaterThan(BigInteger compareValue) {
        return this.value.compareTo(compareValue) > 0;
    }

    public BigInteger value() {
        return value;
    }
}
