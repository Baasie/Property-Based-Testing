package com.baasie.pbt.part3;

import java.math.BigInteger;
import java.util.UUID;

/**
 * Created by kenny on 8-5-17.
 */
public class Product {

    private UUID uuid;
    private ProductName name;
    private Grams grams;

    public Product(UUID uuid, ProductName name, Grams grams) {
        this.uuid = uuid;
        this.name = name;
        this.grams = grams;
    }

    public Grams grams() {
        return grams;
    }

}
