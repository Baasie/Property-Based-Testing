package com.baasie.pbt.part1;

import com.google.common.base.Preconditions;

/**
 * Created by kenny on 2-5-17.
 */
public class PostalParcel {
    public static final double MAX_DELIVERY_COSTS = 4.99;
    public static final double MIN_DELIVERY_COSTS = 1.99;

    private int weight;
    private String uuid;

    public PostalParcel(String uuid, int weight) {
        Preconditions.checkArgument(weight > 0, "Grams not acceptable, less than 0.");
        this.uuid = uuid;
        this.weight = weight;
    }

    public double deliveryCosts() {
        if (weight > 20) {
            return MAX_DELIVERY_COSTS;
        }
        return MIN_DELIVERY_COSTS;
    }
}
