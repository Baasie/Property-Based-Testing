package com.baasie.pbt.part2;

import com.google.common.base.Preconditions;

import java.util.UUID;

/**
 * Created by kenny on 8-5-17.
 */
public class Product {

    private UUID uuid;
    private String name;
    private int weight;

    public Product(UUID uuid, String name, int weight) {
        Preconditions.checkArgument(weight > 0, "Grams not acceptable, less than 0.");
        this.uuid = uuid;
        this.name = name;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

}
