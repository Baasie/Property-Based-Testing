package com.baasie.pbt.part2;

import java.util.List;
import java.util.UUID;

/**
 * Created by kenny on 2-5-17.
 */
public class PostalParcel {
    public static final double MAX_DELIVERY_COSTS = 4.99;
    public static final double MIN_DELIVERY_COSTS = 1.99;

    private UUID uuid;
    private List<Product> products;

    public PostalParcel(UUID uuid, List<Product> products) {
        this.uuid = uuid;
        this.products = products;
    }

    public double deliveryCosts() {
        if (weight() > 20) {
            return MAX_DELIVERY_COSTS;
        }
        return MIN_DELIVERY_COSTS;
    }

    public int weight() {
        return products.stream().map(Product::getWeight).mapToInt(Integer::intValue).sum();
    }
}
