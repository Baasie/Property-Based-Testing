var toUpperCase = function (input) {
    if(typeof input === 'string' && input) {
        return input.trim().toUpperCase();
    }
    return "default"
};

exports.toUpperCase = toUpperCase;