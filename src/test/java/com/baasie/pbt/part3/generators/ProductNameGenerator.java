package com.baasie.pbt.part3.generators;

import com.baasie.pbt.part3.ProductName;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/**
 * Created by kenny on 8-5-17.
 */
public class ProductNameGenerator extends Generator<ProductName> {


    public ProductNameGenerator() {
        super(ProductName.class);
    }

    public ProductName generate(SourceOfRandomness random, GenerationStatus status) {
        StringBuilder sb = new StringBuilder(ProductName.CAPACITY);
        for (int i = 0; i < ProductName.CAPACITY; i++) {
            int randomIndex = random.nextInt(ProductName.ALL_MY_CHARS.length());
            sb.append(ProductName.ALL_MY_CHARS.charAt(randomIndex));
        }
        return new ProductName(sb.toString());
    }
}
