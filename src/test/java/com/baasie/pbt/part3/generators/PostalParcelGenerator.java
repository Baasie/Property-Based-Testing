package com.baasie.pbt.part3.generators;

import com.baasie.pbt.part2.generators.InWeightRange;
import com.baasie.pbt.part3.PostalParcel;
import com.baasie.pbt.part3.Product;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kenny on 8-5-17.
 */
public class PostalParcelGenerator extends Generator<PostalParcel> {
    private int minWeight = 1;
    private int maxWeight = Integer.MAX_VALUE;

    public PostalParcelGenerator() {
        super(PostalParcel.class);
    }


    public PostalParcel generate(SourceOfRandomness sourceOfRandomness, GenerationStatus generationStatus) {
        return new PostalParcel(gen().make(UUIDGenerator.class).generate(sourceOfRandomness, generationStatus),
                generateProducts(sourceOfRandomness, generationStatus));
    }

    private List<Product> generateProducts(SourceOfRandomness sourceOfRandomness, GenerationStatus generationStatus) {
        ProductGenerator productGenerator = gen().make(ProductGenerator.class);
        List<Product> products = new ArrayList<>();
        int randomTotalWeight = sourceOfRandomness.nextInt(minWeight, maxWeight);
        while (randomTotalWeight > 0) {
            int maxWeight = sourceOfRandomness.nextInt(1, randomTotalWeight);
            productGenerator.configureMaxWeight(maxWeight);
            products.add(productGenerator.generate(sourceOfRandomness, generationStatus));
            randomTotalWeight = randomTotalWeight - maxWeight;
        }
        return products;
    }

    public void configure(InWeightRange range) {
        this.minWeight = range.minWeight();
        this.maxWeight = range.maxWeight();
    }
}
