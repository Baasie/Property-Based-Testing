package com.baasie.pbt.part3.generators;

import com.baasie.pbt.part3.Product;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/**
 * Created by kenny on 8-5-17.
 */
public class ProductGenerator extends Generator<Product> {
    private int maxWeight = Integer.MAX_VALUE;
    private int minWeight = 1;

    public ProductGenerator() {
        super(Product.class);
    }

    public Product generate(SourceOfRandomness sourceOfRandomness, GenerationStatus generationStatus) {
        GramsGenerator gramsGenerator = gen().make(GramsGenerator.class);
        gramsGenerator.configure(minWeight, maxWeight);
        return new Product(
                gen().make(UUIDGenerator.class).generate(sourceOfRandomness, generationStatus),
                gen().make(ProductNameGenerator.class).generate(sourceOfRandomness, generationStatus),
                gramsGenerator.generate(sourceOfRandomness, generationStatus));
    }

    public void configureMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }
}
