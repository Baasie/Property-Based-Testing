package com.baasie.pbt.part3.generators;

import com.baasie.pbt.part3.Grams;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created by kenny on 8-5-17.
 */
public class GramsGenerator extends Generator<Grams> {
    private int maxInt = Integer.MAX_VALUE;
    private int minInt = 0;

    public GramsGenerator() {
        super(Grams.class);
    }

    public Grams generate(
            SourceOfRandomness random,
            GenerationStatus status) {

        return new Grams(BigInteger.valueOf(random.nextInt(minInt, maxInt)));
    }

    public void configure(int minInt, int maxInt) {
        this.minInt = Math.abs(minInt);
        this.maxInt = maxInt;
    }
}
