package com.baasie.pbt.part2;

import com.baasie.pbt.part2.generators.InWeightRange;
import com.pholser.junit.quickcheck.Mode;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;


/**
 * Created by kenny on 2-5-17.
 */
@RunWith(JUnitQuickcheck.class)
public class PostalParcelPBTTest {

    @Property(mode = Mode.EXHAUSTIVE)
    public void deliveryCostsShouldBeMaxWhenWeightIsGreaterThan20(@InWeightRange(minWeight = 21) PostalParcel postalParcel) {
        assumeThat(postalParcel.weight(), greaterThan(20));

        assertThat(postalParcel.deliveryCosts(), equalTo(com.baasie.pbt.part1.PostalParcel.MAX_DELIVERY_COSTS));
    }

    @Property(trials = 25)
    public void deliveryCostsShouldBeMinWhenWeightIsLessThanOrEqualTo20(@InWeightRange(maxWeight = 20) PostalParcel postalParcel) {
        assumeThat(postalParcel.weight(), is(both(greaterThan(0)).and(lessThanOrEqualTo(20))));

        assertThat(postalParcel.deliveryCosts(), equalTo(com.baasie.pbt.part1.PostalParcel.MIN_DELIVERY_COSTS));
    }

}
