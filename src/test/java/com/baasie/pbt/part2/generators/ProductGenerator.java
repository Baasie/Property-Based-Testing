package com.baasie.pbt.part2.generators;

import com.baasie.pbt.part2.Product;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/**
 * Created by kenny on 8-5-17.
 */
public class ProductGenerator extends Generator<Product> {
    private int maxWeight = Integer.MAX_VALUE;
    private int minWeight = 1;

    public ProductGenerator() {
        super(Product.class);
    }

    public Product generate(SourceOfRandomness sourceOfRandomness, GenerationStatus generationStatus) {
        NonNegativeIntsGenerator nonNegativeIntsGenerator = gen().make(NonNegativeIntsGenerator.class);
        nonNegativeIntsGenerator.configure(minWeight, maxWeight);
        return new Product(gen().make(UUIDGenerator.class).generate(sourceOfRandomness, generationStatus),
        gen().make(MyStringGenerator.class).generate(sourceOfRandomness, generationStatus),
                nonNegativeIntsGenerator.generate(sourceOfRandomness, generationStatus));
    }

    public void configureMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }
}
