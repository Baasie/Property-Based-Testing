package com.baasie.pbt.part2.generators;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

import java.util.UUID;

/**
 * Created by kenny on 16-5-17.
 */
public class UUIDGenerator extends Generator<UUID> {

    public UUIDGenerator() {
        super(UUID.class);
    }

    public UUID generate(SourceOfRandomness sourceOfRandomness, GenerationStatus generationStatus) {
        return UUID.randomUUID();
    }
}
