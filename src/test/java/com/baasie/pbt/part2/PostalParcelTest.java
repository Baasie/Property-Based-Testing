package com.baasie.pbt.part2;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;

/**
 * Created by kenny on 16-5-17.
 */
public class PostalParcelTest {

    @Test
    public void deliveryCostsShouldBeMaxWhenWeightIsGreaterThan20(){
        PostalParcel postalParcel = generatePostalParcel(22);
        assumeThat(postalParcel.weight(), greaterThan(20));

        assertThat(postalParcel.deliveryCosts(), equalTo(com.baasie.pbt.part1.PostalParcel.MAX_DELIVERY_COSTS));
    }

    @Test
    public void deliveryCostsShouldBeMinWhenWeightIsLessThanOrEqualTo20(){
        PostalParcel postalParcel = generatePostalParcel(18);
        assumeThat(postalParcel.weight(), is(both(greaterThan(0)).and(lessThanOrEqualTo(20))));

        assertThat(postalParcel.deliveryCosts(), equalTo(com.baasie.pbt.part1.PostalParcel.MIN_DELIVERY_COSTS));
    }

    private PostalParcel generatePostalParcel(int weight) {
        return new PostalParcel(UUID.randomUUID(), generateProducts(weight));
    }

    private List<Product> generateProducts(int weight) {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            products.add(new Product(UUID.randomUUID(), "Name", weight/4));
        }
        return products;
    }
}
