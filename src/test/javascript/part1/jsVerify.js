const jsc = require("jsverify");
const stringUtils = require("../../../main/javascript/part1/StringUtils");

describe("stringUtils toUpperCase", function () {
    jsc.property("should upperCase and trim all strings", "nestring", function (string) {
        console.log(string);
        return string.trim().toUpperCase() === stringUtils.toUpperCase(string);
    });

    jsc.property("should return defaults for falsy inputs", "falsy", function (falsy) {
        console.log(falsy);
        return 'default' === stringUtils.toUpperCase(falsy);
    });
});